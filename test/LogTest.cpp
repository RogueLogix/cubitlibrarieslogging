#include <Cubit/Libraries/Logging/Log.hpp>
#include <iostream>
#include <iomanip>
#include <thread>

namespace Cubit::Libraries::Logging {
    std::string datetime();
}

int main() {
    namespace lg = Cubit::Libraries::Logging;
    lg::Log mainLog;
    {
        lg::OutputStreamOutput output(&std::cout);
        mainLog.addOutput(output);
        
        lg::OutputFile file("logs", "testLog");
        mainLog.addOutput(file);
    }
    
    mainLog.println("Hello World!");
    
    std::time_t result = std::time(nullptr);
    auto tm = std::localtime(&result);
    mainLog.println(lg::datetime());
    
    
    int iterations = 1'000;
    std::chrono::nanoseconds avgTime;
    auto start = std::chrono::system_clock::now();
    auto end = std::chrono::system_clock::now();
    for (int i = 0; i < iterations; ++i) {
//        start = std::chrono::system_clock::now();
//        std::cout << "TestLog\n";
//        end = std::chrono::system_clock::now();
//        auto baseline = end - start;
        
        start = std::chrono::system_clock::now();
        mainLog.println("TestLog");
        end = std::chrono::system_clock::now();
        auto bench = end - start;
        
        avgTime += bench;
    }
    
    avgTime /= iterations;
    
    mainLog.println(std::to_string(avgTime.count()));
    
    mainLog.print(std::to_string(std::thread::hardware_concurrency()));
}